import { Injectable } from '@nestjs/common';
import { PrismaCrudService } from 'nestjs-prisma-crud';

@Injectable()
export class NestPrismauserService extends PrismaCrudService {
  constructor() {
    super({
      model: 'post',
      allowedJoins: ['comments.author'],
    });
  }
}
