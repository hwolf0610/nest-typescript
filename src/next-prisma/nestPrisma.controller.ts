import { Controller, Get, Query } from '@nestjs/common';
import { NestPrismauserService } from './nestPrisma.service';
import { ApiTags } from '@nestjs/swagger';

@ApiTags('prisma')
@Controller('post')
export class NestPrismaController {
  constructor(private readonly postService: NestPrismauserService) {}

  // @Get()
  // async findMany(@Query('crudQuery') crudQuery: string) {
  //   const matches = await this.postService.findMany(crudQuery);
  //   return matches;
  // }
}
