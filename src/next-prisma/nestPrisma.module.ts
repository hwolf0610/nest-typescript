import { Module } from '@nestjs/common';
import { NestPrismaController } from './nestPrisma.controller';

import { NestPrismauserService } from './nestPrisma.service';

@Module({
  imports: [],
  controllers: [NestPrismaController],
  providers: [NestPrismauserService],
})
export class NestPrismaModule {}
