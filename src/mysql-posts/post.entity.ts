import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class PostEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @Column()
  detail: string;

  @Column()
  author: string;

  @Column({ default: new Date().toString() })
  published: string;

  @Column({ default: true })
  isActive: boolean;
}
