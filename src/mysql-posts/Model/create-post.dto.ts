export class CreatePostDto {
  title: string;
  detail: string;
  author: string;
}
