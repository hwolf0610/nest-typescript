import { MailerService } from '@nestjs-modules/mailer';
import { Injectable } from '@nestjs/common';

@Injectable()
export class MailerNestService {
  constructor(private readonly mailerService: MailerService) {}

  sendEmailStarter() {
    return this.mailerService.sendMail({
      to: 'hwolf0610@gmail.com', // List of receivers email address
      from: process.env.SMTP_AUTH_USER, //  'user@localhost', // Senders email address
      subject: 'Testing Nest Mailer module with template ✔',
      template: 'index', // The `.twig` extension is appended automatically.
      context: {
        // Data to be sent to template engine.
        code: 'cf1a3f828287',
        username: 'Harry Naruto',
      },
    });
  }
}
