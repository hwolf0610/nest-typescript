import { Controller, Get } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';

import { MailerNestService } from './mailer.service';

@ApiTags('nest-mailer')
@Controller('nest-mailer')
export class MailerNestController {
  constructor(private readonly mailerNestService: MailerNestService) {}

  @Get('email')
  sendEmail() {
    return this.mailerNestService.sendEmailStarter();
  }
}
