import { Module } from '@nestjs/common';
import { MailerModule } from '@nestjs-modules/mailer';

import { MailerNestController } from './mailer.controller';
import { MailerNestService } from './mailer.service';
import { MailerAdapter } from './mailer.adapter';

@Module({
  imports: [
    MailerModule.forRoot({
      transport: {
        host: process.env.SMTP_HOST || 'localhost',
        port: parseInt(process.env.SMTP_PORT, 10) || 1025,
        secure: process.env.SMTP_SECURE === 'true',
        ignoreTLS: process.env.SMTP_SECURE !== 'false',
        auth: {
          user: process.env.SMTP_AUTH_USER || 'username',
          pass: process.env.SMTP_AUTH_PASS || 'password',
        },
      },
      template: {
        dir: `${process.cwd()}/src/mailer/templates/`,
        adapter: new MailerAdapter(),
      },
    }),
  ],
  providers: [MailerNestService],
  controllers: [MailerNestController],
})
export class MailerNestModule {}
