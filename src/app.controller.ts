import {
  Controller,
  Get,
  Post,
  HttpCode,
  Header,
  Redirect,
  Param,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';

@ApiTags('cats')
@Controller('cats')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Get('/currentDate')
  getCurrentDate(): string {
    return this.appService.getCurrentDate();
  }

  @Get('/findall')
  findAll(): string {
    return 'This action returns all cats';
  }

  @Get(':id')
  findOne(@Param('id') id: string): string {
    return `This action returns a #${id} cat`;
  }

  @Post()
  @HttpCode(201)
  @Header('Cache-Control', 'none')
  // eslint-disable-next-line @typescript-eslint/ban-types
  create(): object {
    return { data: 'This action adds a new cat' };
  }
}
