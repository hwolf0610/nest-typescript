import { NestFactory } from '@nestjs/core';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import * as basicAuth from 'express-basic-auth';
// import cors from 'cors';

import { AppModule } from './app.module';

import { PrismaService } from './prisma/prisma.service';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  const prismaService: PrismaService = app.get(PrismaService);
  prismaService.enableShutdownHooks(app);

  // Sometime after NestFactory add this to add HTTP Basic Auth
  app.use(
    ['/api', '/api-json'],
    basicAuth({
      challenge: true,
      users: {
        harryNest: 'p4ssw0rd',
      },
    }),
  );

  const config = new DocumentBuilder()
    .setTitle('Cats example')
    .setDescription('The cats API description')
    .setVersion('1.0')
    .addTag('cats')
    .addTag('prisma')
    .addTag('mysql-users')
    .addTag('mysql-posts')
    .addTag('nest-mailer')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document, {
    customSiteTitle: 'My App documentation',
  });

  // app.use(cors());
  await app.listen(3000);
}
bootstrap();
