import { Module } from '@nestjs/common';

import { TypeOrmModule } from '@nestjs/typeorm';
// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';

// import { PrismaCrudModule } from 'nestjs-prisma-crud';

import { AppController } from './app.controller';

import { UsersModule } from './mysql-users/users.module';
import { PostsModule } from './mysql-posts/posts.module';

import { MailerNestModule } from './mailer/mailer.module';

import { PrismaModule } from './prisma/prisma.module';
import { NestPrismaModule } from './next-prisma/nestPrisma.module';

import { AppService } from './app.service';
// import { PrismaService } from './prisma/prisma.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mysql',
      host: process.env.MYSQL_HOST,
      port: 3306,
      username: process.env.MYSQL_USER,
      password: process.env.MYSQL_PASSWORD,
      database: process.env.MYSQL_DATABASE,
      autoLoadEntities: true,
      synchronize: true,
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'client'), // <-- path to the static files
    }),
    // PrismaCrudModule.register({
    //   prismaService: PrismaService,
    // }),
    UsersModule,
    PostsModule,
    MailerNestModule,
    PrismaModule,
    NestPrismaModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
