import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getCurrentDate(): string {
    const currentDate = new Date();
    return 'Now is : ' + currentDate;
  }
}
